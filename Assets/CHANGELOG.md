# Changelog
All notable changes to this project will be documented in this file.

## [0.0.7] - 03-11-2019
### Changed
	-change name method `LoadResourceLocationsAddressable` to `LoadResourceLocationAddressables`