# Addressable Extend

## Installation

```bash
"com.yenmoc.addressable-extend":"https://gitlab.com/yenmoc/addressable-extend"
or
npm publish --registry=http://localhost:4873
```

## Usages

* 1
```csharp
 public static UniTask<T> LoadAddressable<T>(object key) where T : class
{
    var completionSource = new UniTaskCompletionSource<T>();
    Addressables.LoadAssetAsync<T>(key).Completed += x => completionSource.TrySetResult(x.Result);
    return completionSource.Task;
}
```

```csharp
GameObject image = await LazyAddressable.LoadAddressable<GameObject>("Image");
Addressables.Release(image);
   "`GameObject image` still exist after the data has been released"
=> "keep use image or set image equal null after"



[CreateAssetMenu]
[Serializable]
public class StringTest : ScriptableObject
{
    public string data;
}

StringTest data = await LazyAddressable.LoadAddressable<StringTest>("data");
Addressables.Release(data);
   "`StringTest data` still exist after the data has been released"
=> "keep use data or set data equal null after"
```


```csharp
public AssetReference image;
GameObject go = await image.LoadAddressable<GameObject>(); //hold gameobject but not generate intance
GameObject instanceGo = Instantiate(go, transform);			//generate intance

"can release with :"
	-image.ReleaseInstance(go);
    -image.ReleaseAsset();
    -Addressables.Release(go);
    -Addressables.ReleaseInstance(go);

"`instanceGo` still exist after the data has been released"
```


```csharp
GameObject go = await image.InstantiateAddressable(transform);

"can release with :"
    -Addressables.Release(go);
    -Addressables.ReleaseInstance(go);

"`go` will be destroyed at the same time as the release of addressable"
```


```csharp
SceneInstance scene = await LazyAddressable.LoadSceneAddressable("name scene");

Addressables.UnloadSceneAsync(scene); //unload scene
Addressables.Release(scene);			//release
```

```csharp
 public AssetLabelReference label;
 IList<IResourceLocation> locations = await LazyAddressable.LoadResourceLocationsAddressable(lable.labelString);

 //can Load or Instance from locations
for (int i = 0; i < _locations.Count; i++)
{
    gos[i] = await _locations[i].LoadAddressable<GameObject>();
}

gos = new GameObject[_locations.Count];
for (int i = 0; i < _locations.Count; i++)
{
    gos[i] = await _locations[i].LoadAddressable<GameObject>();
}


gos = new GameObject[_locations.Count];
for (int i = 0; i < _locations.Count; i++)
{
    gos[i] = await _locations[i].InstantiateAddressable(transform);
}
```


